﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Solution
{
    class Defibrillator
    {
        public float x, y;

        public string name;

        public float distance(float personX, float personY)
        {
            //float tempX = (x - personX) * ((float)Math.Cos((personY + y)) / 2);
            //float tempY = (y - personY);
            float tempX = (personX - x) * (float)Math.Cos((personY + y) / 2);
            float tempY = (personY - y);
            return (float)Math.Sqrt(Math.Pow(tempX, 2) + Math.Pow(tempY, 2)) * 6371;
        }

        public Defibrillator(string s, float a, float b)
        {
            name = s;
            x = toRadian(a);
            y = toRadian(b);
        }
    }

    static void Main(string[] args)
    {
        float LON = toRadian(float.Parse(Console.ReadLine()));
        float LAT = toRadian(float.Parse(Console.ReadLine()));
        int N = int.Parse(Console.ReadLine());
        string output = "";
        Defibrillator[] defibrillators = new Defibrillator[N];

        for (int i = 0; i < N; i++)
        {
            string DEFIB = Console.ReadLine();
            string[] temp = DEFIB.Split(';');
            defibrillators[i] = new Defibrillator(temp[1],
                float.Parse(temp[temp.Length - 2].Replace(',', '.')),
                float.Parse(temp[temp.Length - 1].Replace(',', '.')));
        }

        float closest = float.MaxValue;
        foreach (var d in defibrillators)
        {
            float dist = d.distance(LON, LAT);
            if (dist < closest)
            {
                closest = dist;
                output = d.name;
            }
        }
        Console.WriteLine(output);
    }


    public static float toRadian(float degrees)
    {
        return (float) (Math.PI / 180) * degrees;
    }
}
